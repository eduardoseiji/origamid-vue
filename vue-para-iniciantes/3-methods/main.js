const vm = new Vue({
    el: '#app',
    data: {
        total: 0,
        instrumento: ''
    },
    methods: {
        incrementar(){
            this.total++;
        },
        diminuir(){
            this.total--;
        },
        mudarNome(instrumento) {
            this.instrumento = instrumento;
        },
        verEvento(e) {
            console.log(e);            
        },
        teste() {
            console.log('Metodo ativado');            
        }
    }
});