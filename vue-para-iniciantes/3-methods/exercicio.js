
 /* 
  Utilizando a API   https://api.iextrading.com/1.0/stock/aapl/quote
  Crie um método que faça o fetch da resposta acima
  O método deve ser ativado ao clique no botão Ver Preço
*/
const vm = new Vue({
    el: '#app',
    data: {
        acoes: {}
    },
    methods: {
        verInfos() {
            fetch('https://cloud.iexapis.com/v1')
            .then(r => r.json())
            .then(r2 => {                
                   this.acoes = r2;           
            });
        }
    }
});
/*
  Coloque a resposta json do fetch em uma data e mostre o valor de lalestPrice, latestTime e o companyName na interface. 
*/