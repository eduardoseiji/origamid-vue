<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <script src="/../../node_modules/vue/dist/vue.min.js"></script>
</head>
<body>
    
    <div id="app">
        <!-- <div>
            {{total}}
            <button @click="incrementar">Incrementar</button>
            <button @click="diminuir">Diminuir</button>
            {{instrumento}}
            <button @click="mudarNome('Violao')">Violão</button>
            <button @click="verEvento">Evento</button>
        </div> -->
        <button @click="verInfos">Ver Infos</button>
        <p>API: {{acoes.openapi}}</p>
    </div>
    <script src="3-methods/exercicio.js"></script>
</body>
</html>