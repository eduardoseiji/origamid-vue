/**
  Utilizando as API's
  https://api.origamid.dev/stock/aapl/quote
  https://api.origamid.dev/stock/googl/quote

  - Compare o marketCap (valor de
  mercado) entre ambas.

  - Mostre o nome das empresas e o valor
  na interace (companyName e marketCap).

  - Mude a cor para verde o nome da empresa com
  maior marketCap e vermelho a com menor.
*/

"use strict"

const vm = new Vue({
    el: "#app",
    data: {
        google: {},
        apple: {}
    },
    methods: {
        fetchGoogle() {
            fetch('https://api.origamid.dev/stock/aapl/quote')
            .then(result => result.json())
            .then(r2 => {
                this.google = r2;
            }); 
        },
        fetchApple() {
            fetch('  https://api.origamid.dev/stock/googl/quote')
            .then(result => result.json())
            .then(r2 => {
                this.apple = r2;
            });            
        },
        puxarDados() {
            this.fetchApple();
            this.fetchGoogle();
        }
    }
});